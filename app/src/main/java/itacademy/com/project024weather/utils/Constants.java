package itacademy.com.project024weather.utils;

public class Constants {

    public static final String BASE_URL = "http://api.openweathermap.org/data/2.5/";

    public static final String URL_IMAGE = "http://openweathermap.org/img/w/%1s.png";

    public static final String URL_WEATHER = "weather";
    public static final String URL_FORECAST = "forecast";

}
