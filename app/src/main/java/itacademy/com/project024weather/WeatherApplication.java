package itacademy.com.project024weather;

import android.app.Application;
import android.content.Context;

import itacademy.com.project024weather.data.NetworkBuilder;
import itacademy.com.project024weather.data.RetrofitService;

public class WeatherApplication extends Application {

    private RetrofitService service;

    @Override
    public void onCreate() {
        super.onCreate();
        service = NetworkBuilder.initService();
    }

    public static WeatherApplication get(Context context) {
        return (WeatherApplication) context.getApplicationContext();
    }

    public RetrofitService getService() {
        return service;
    }

}
