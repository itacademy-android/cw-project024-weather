package itacademy.com.project024weather.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import itacademy.com.project024weather.R;
import itacademy.com.project024weather.WeatherApplication;
import itacademy.com.project024weather.data.RetrofitService;
import itacademy.com.project024weather.data.models.GeneralModel;
import itacademy.com.project024weather.utils.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BishkekFragment extends Fragment {

    private TextView tvCity, tvUpdate, tvTemperature, tvDetails;
    private ImageView imageView;
    private FrameLayout progressBar;

    private RetrofitService service;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        service = WeatherApplication.get(getContext()).getService();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_weather, container, false);

        progressBar = view.findViewById(R.id.progressBar);

        tvCity = view.findViewById(R.id.tvCity);
        tvUpdate = view.findViewById(R.id.tvUpdate);
        tvTemperature = view.findViewById(R.id.tvTemperature);
        tvDetails = view.findViewById(R.id.tvDetails);
        imageView = view.findViewById(R.id.imageView);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getWeather();
    }

    private void getWeather() {
        progressBar.setVisibility(View.VISIBLE);

        service.getWeatherByName("Bishkek", getString(R.string.API_KEY), "metric")
                .enqueue(new Callback<GeneralModel>() {
                    @Override
                    public void onResponse(@NonNull Call<GeneralModel> call, @NonNull Response<GeneralModel> response) {
                        if (response.isSuccessful() && response.body() != null) {
                            updateData(response.body());
                        } else {
                            Toast.makeText(getContext(), "Error occurred", Toast.LENGTH_SHORT).show();
                        }
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onFailure(@NonNull Call<GeneralModel> call, @NonNull Throwable t) {
                        Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                        progressBar.setVisibility(View.GONE);
                    }
                });
    }

    private void updateData(GeneralModel model) {
        tvCity.setText(model.getName());
        tvUpdate.setText(String.valueOf(model.getDt()));
        tvTemperature.setText(String.valueOf(model.getMain().getTemp()));
        tvDetails.setText(model.getWeatherList().get(0).getDescription());

        String icon = model.getWeatherList().get(0).getIcon();
        String imageURL = String.format(Constants.URL_IMAGE, icon);

        Picasso.get().load(imageURL).into(imageView);
    }
}
