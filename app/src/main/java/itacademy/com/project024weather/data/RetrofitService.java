package itacademy.com.project024weather.data;

import itacademy.com.project024weather.data.models.GeneralModel;
import itacademy.com.project024weather.utils.Constants;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RetrofitService {

    @GET(Constants.URL_WEATHER)
    Call<GeneralModel> getWeatherByName(@Query("q") String name,
                                        @Query("appid") String appId,
                                        @Query("units") String units);

}
